import * as express from 'express';
import { Guard, CanActivate, ExecutionContext, HttpStatus } from '@nestjs/common';
import { Reflector, HttpException } from '@nestjs/core';

@Guard()
export class PermissionsGuard implements CanActivate {
    constructor(private readonly reflector: Reflector) {
    }

    canActivate(req: express.Request, context: ExecutionContext): boolean {
        let userPermissions;

        try {
            userPermissions = JSON.parse(req.header("token-claim-permissions") ||
                                         req.header("token-claim-client_permissions"));
        } catch (err) {
            throw new HttpException("Invalid permissions claim", HttpStatus.BAD_REQUEST);
        }

        const permissions = this.reflector.get<string[]>('permissions', context.handler);

        if (!permissions) {
            return true;
        }

        for (let p of permissions) {
            if (!userPermissions.includes(p)) {
                return false;
            }
        }

        return true;
    }
}
