import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";

require("dotenv").config();

import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as express from "express";
import * as morgan from "morgan";
import { NestFactory } from "@nestjs/core";
import { ApplicationModule } from "./modules/app.module";

async function bootstrap() {
    const app: express.Application = express();

    app.use(morgan("dev"));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cors({
        preflightContinue: true
    }));
    app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        if (req.headers[ "origin" ]) {
            res.setHeader("Access-Control-Allow-Origin", req.headers[ "origin" ]);
            res.setHeader("Access-Control-Allow-Credentials", "true");
        }
        next();
    });

    try {
        const nestApp = await NestFactory.create(ApplicationModule, app);
        const options = new DocumentBuilder()
            .setTitle('Api Example')
            .setDescription('Api Example')
            .setVersion('1.0')
            .addBearerAuth()
            .addTag('users')
            .build();
        const document = SwaggerModule.createDocument(nestApp, options);
        SwaggerModule.setup('/api', app, document);
        await nestApp.listen(Number(process.env.PORT));
    } catch (err) {
        console.log(err);
        process.exit(-1);
    }
}

bootstrap();
