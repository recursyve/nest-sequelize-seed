import { Sequelize } from 'sequelize-typescript';
import { User } from "./user/user.model";

export const databaseProviders = [
    {
        provide: 'SequelizeToken',
        useFactory: () => {
            const sequelize = new Sequelize({
                dialect: 'mysql',
                host: process.env.DB_HOST,
                username: process.env.DB_USERNAME,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_NAME,
                logging: false
          });
          sequelize.addModels([ User ]);
          return sequelize;
        }
    }
];
