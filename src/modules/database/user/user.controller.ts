import { Body, Controller, Get, Post } from "@nestjs/common";
import { CreateUserDto } from "./dto/user.dto";
import { UserService } from "./user.service";
import { ApiBearerAuth, ApiUseTags } from "@nestjs/swagger";
import { ValidationPipe } from "../../../pipes/validation.pipe";

@ApiUseTags('users')
@ApiBearerAuth()
@Controller()
export class UserController {

    constructor(private userService: UserService) {}

    @Post()
    async create(@Body(new ValidationPipe()) userDto: CreateUserDto) {
        return {
            user: await this.userService.create(userDto)
        };
    }

    @Get()
    async getAll() {
        return {
            users: await this.userService.findAll()
        };
    }
}
