import { IsNotEmpty, IsString } from "class-validator";
import {ApiModelProperty} from "@nestjs/swagger";

export class CreateUserDto {

    @ApiModelProperty({
        required: true
    })
    @IsString()
    @IsNotEmpty()
    username: string;

    @ApiModelProperty({
        required: true
    })
    @IsString()
    @IsNotEmpty()
    password: string;
}
