import { Module } from "@nestjs/common";
import { DatabaseModule } from "../database.module";
import { UserController } from "./user.controller";
import { userProviders } from "./user.providers";
import { UserService } from "./user.service";

@Module({
    modules: [ DatabaseModule ],
    controllers: [ UserController ],
    components: [ ...userProviders, UserService ],
    exports: [ UserService ]
})
export class UserModule { }
