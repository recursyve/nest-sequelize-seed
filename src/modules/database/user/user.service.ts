import { Component, Inject } from '@nestjs/common';
import { User } from './user.model';
import {CreateUserDto} from "./dto/user.dto";

@Component()
export class UserService {
    constructor(@Inject('UserRepository') private readonly userRepository: typeof User) {}

    async create(user: CreateUserDto): Promise<User> {
        return this.userRepository.create(user);
    }

    async findAll(): Promise<User[]> {
        return this.userRepository.findAll();
    }
}
