import { Module } from "@nestjs/common";
import { UserModule } from "./database/user/user.module";

@Module({
    modules: [ UserModule ]
})
export class ApplicationModule {}
