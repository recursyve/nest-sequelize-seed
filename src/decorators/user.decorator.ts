import { createRouteParamDecorator } from '@nestjs/common';

const User = createRouteParamDecorator((data, req) => {
    return req.user;
});
