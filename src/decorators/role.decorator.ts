import { createRouteParamDecorator } from '@nestjs/common';

const Role = createRouteParamDecorator((data, req) => {
    return req.user.role;
});
